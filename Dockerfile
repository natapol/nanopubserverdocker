# Base image
#
# VERSION   1.9
# docker run --name test -p 49157:8080 --link kickass_sinoussi:mongodb -d natapol/test

FROM iron/java:1.7.0-dev

ENV DB_HOST=mongodb
ENV DB_PORT=27017
ENV DB_NAME=nanopub-server
ENV JAVA_HOME=/usr/lib/jvm/java-7-openjdk-amd64

MAINTAINER Natapol Pornputtapong <natapol.por@gmail.com>

# install prerequisite
RUN apk update && \
  && apk upgrade \
  && apk add maven \
    git \
  && mkdir /src && cd /src \
  && git clone https://github.com/tkuhn/nanopub-server.git \
  && cp nanopub-server/src/main/resources/ch/tkuhn/nanopub/server/conf.properties nanopub-server/src/main/resources/ch/tkuhn/nanopub/server/local.conf.properties \
  && sed -i 's|admin=|admin='$DB_ADMIN'|' nanopub-server/src/main/resources/ch/tkuhn/nanopub/server/local.conf.properties \
  && sed -i 's|mongodb\.host=localhost|mongodb\.host='$DB_HOST'|' nanopub-server/src/main/resources/ch/tkuhn/nanopub/server/local.conf.properties \
  && sed -i 's|mongodb\.port=27017|mongodb\.port='$DB_PORT'|' nanopub-server/src/main/resources/ch/tkuhn/nanopub/server/local.conf.properties \
  && sed -i 's|mongodb\.dbname=nanopub-server|mongodb\.dbname='$DB_NAME'|' nanopub-server/src/main/resources/ch/tkuhn/nanopub/server/local.conf.properties \
  && cd nanopub-server \
  && mvn package \
  && apt-get remove -y git \
  && apt-get autoremove -y \
  && apt-get autoclean \
  && apt-get purge \
  && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

EXPOSE 8080
WORKDIR /src/nanopub-server
CMD ["mvn", "jetty:run"]
